import re

class irc_hello(object):
    """ Say hello, when quoted """

    commands = ['PRIVMSG']

    def __call__(self, client, msg):
        
        ident = re.match(r'(?P<nickname>.+)\!\~(?P<ident>.+)\@(?P<ip>.+)', msg.prefix)
        ident = ident.groupdict()
        
        channel   = msg.params[0]
        if client.nick in msg.params[1]:
            if ['nerve', 'sekretoree'] in [ident['nickname']]:
                client.msg(channel, "Chya-ssu "+ident['nickname']+"-chan!")
            else:
                client.msg(channel, "Chya-ssu "+ident['nickname']+"-kun!")

