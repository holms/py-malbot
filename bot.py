# -*- coding: utf-8 -*-

try:
	import config
except Exception:
	print "rename config.example.py to config.py and edit your nick name data"
	sys.exit()

from geventirc import Client, handlers, message

from ext.irc_hello import irc_hello

def join_chans_handler(client, msg):
    for chan in config.channels:
        client.send_message(message.Join(chan))

irc = Client(config.server, config.nick, config.port)

irc.add_handler(handlers.PingHandler())
irc.add_handler(handlers.print_handler)
irc.add_handler(join_chans_handler, '001')
irc.add_handler(handlers.NickServHandler(config.nick, config.password))
irc.add_handler(irc_hello())

irc.start()
irc.join()


